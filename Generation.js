/**
 *`Generation`: A smart way to deal with generators.
 *
 * @class Generation
 */
class Generation {
  /**
   *Creates an instance of `Generation`.
   * @param {GeneratorFunction} generator
   * @memberof Generation
   */
  constructor (generator) {
    this._gen = generator.bind(this)
  }

  /**
   *Returns a new `Generation` that applies certain arguments to its generator.
   *
   * @param {any[]} args
   * @returns {Generation} The applied generation.
   * @memberof Generation
   */
  apply (args) {
    const gen = this._gen
    return new Generation(function * () {
      for (const el of gen.apply(this, args)) yield el
    })
  }

  /**
   *Returns a new `Generation` that calls its generator with certain arguments.
   *
   * @param {any[]} ...args
   * @returns {Generation} The called generation.
   * @memberof Generation
   */
  call () {
    return this.apply(arguments)
  }

  /**
   *Returns a new `Generation` that groups its yields in arrays of certain length.
   *
   * @param {number} n
   * @returns {Generation} The grouper generation
   * @memberof Generation
   */
  each (n) {
    const gen = this._gen
    return new Generation(function * () {
      let g = []
      for (const el of gen()) {
        g.push(el)
        if (g.length >= n) {
          yield g
          g = []
        }
      }
      if (g.length) yield g
    })
  }

  /**
   *Returns a new Generation that yields only the yields that applies to a certain condition.
   *
   * @param {Function} func
   * @returns {Generation} The conditional generation.
   * @memberof Generation
   */
  filter (func) {
    const gen = this._gen
    return new Generation(function * () {
      let i = -1
      for (const el of gen()) if (func(el, ++i)) yield i
    })
  }

  /**
   *Returns a new `Generation` that yields only the first `n` yields then breaks.
   *
   * @param {number} n
   * @returns {Generation} The limiter generation.
   * @memberof Generation
   */
  first (n) {
    const gen = this._gen
    return new Generation(function * () {
      let i = 0
      for (const el of gen()) {
        yield el
        if (++i >= n) break
      }
    })
  }

  /**
   *Applies a function on each of the yields and returns a new `Promise` that resolves to `void`
   *when it breaks.
   *
   * @param {Function} func
   * @returns {Promise<void>} The break promise
   * @memberof Generation
   */
  async forEach (func) {
    let i = 0
    const _break = () => _break
    for (const el of this._gen()) if (func(el, i++, _break) === _break) return false
    return true
  }

  /**
   *Returns a promise that resolves to a certain yield identified by index (as if the `Generation`
   *is an array) then breaks.
   *
   * @param {number} index
   * @returns {Promise<any>} The getter promise.
   * @memberof Generation
   */
  async get (index) {
    let i = 0
    for (const el of this._gen()) if (i++ === index) return el
  }

  /**
   *Returns a new `Generation` that yields the yields in array groups according to a condition
   *deciding wheather they are alike.
   *
   * @param {Function} func
   * @returns {Generation} The grouper generation.
   * @memberof Generation
   */
  group (func) {
    const gen = this._gen
    return new Generation(function * () {
      let g = []
      let cur, prev
      for (const el of gen()) {
        const val = func(el, prev)
        if (!g.length || cur === val) g.push(el)
        else {
          yield g
          g = [el]
        }
        cur = val
        prev = el
      }
      if (g.length) yield g
    })
  }

  /**
   *Returns a new `Promise` that resolves to a boolean value indicating if a certain value or object is
   *yielded then breaks.
   *
   * @param {any} element
   * @returns {Promise<boolean>} The boolean indicator.
   * @memberof Generation
   */
  async has (element) {
    for (const el of this._gen()) if (el === element) return true
    return false
  }

  /**
   *Returns a new `Generation` that yields the returns of a function applied to the yields.
   *
   * @param {Function} func
   * @returns {Generation} The altered generation.
   * @memberof Generation
   */
  map (func) {
    const gen = this._gen
    return new Generation(function * () {
      let i = 0
      for (const el of gen()) yield func(el, i++)
    })
  }

  /**
   *Returns a new `Generation` that yields reduction of the yields by certain operation once.
   *
   * @param {Function} func
   * @param {any} init
   * @returns {Muatgen} The reducer generation.
   * @memberof Generation
   */
  reduce (func, init) {
    const gen = this._gen
    return new Generation(function * () {
      let res = init
      for (const el of gen()) {
        if (res != null) res = func(res, el)
        else res = el
        yield res
      }
    })
  }

  /**
   *Returns a new `Promise` that resolves to an array representation of the yields when it breaks.
   *
   * @returns {Promise<any[]>} The array promise
   * @memberof Generation
   */
  async toArray () {
    return Array.from(this._gen())
  }

  /**
   *Returns a new `Generation` that makes sure not to repeat yielded values.
   *
   * @returns {Generation} The unified generation.
   * @memberof Generation
   */
  unique () {
    const gen = this._gen
    return new Generation(function * () {
      let found = []
      for (const el of gen()) {
        if (found.indexOf(el) === -1) {
          yield el
          found.push(el)
        }
      }
    })
  }

  /**
   *Returns a new `Generation` that yields until a certain condition is `true` then breaks.
   *
   * @param {Function} cond
   * @returns {Generation} The conditional generation.
   * @memberof Generation
   */
  until (cond) {
    const gen = this._gen
    return new Generation(function * () {
      let i = 0
      for (const el of gen()) {
        if (cond(el, i++)) return
        yield el
      }
    })
  }

  /**
   *Returns a new `Generation` that yields until a certain condition is `false` then breaks.
   *
   * @param {Function} cond
   * @returns {Generation} The conditional generation.
   * @memberof Generation
   */
  while (cond) {
    return this.until((el, i) => !cond(el, i))
  }
}

if (module) module.exports = Generation
else this.Generation = Generation
