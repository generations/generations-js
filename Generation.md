<a name="Generation"></a>

## Generation
**Kind**: global class  

* [Generation](#Generation)
    * [new Generation()](#new_Generation_new)
    * _instance_
        * [.apply(args)](#Generation+apply) ⇒ [<code>Generation</code>](#Generation)
        * [.call()](#Generation+call) ⇒ [<code>Generation</code>](#Generation)
        * [.each(n)](#Generation+each) ⇒ [<code>Generation</code>](#Generation)
        * [.filter(func)](#Generation+filter) ⇒ [<code>Generation</code>](#Generation)
        * [.first(n)](#Generation+first) ⇒ [<code>Generation</code>](#Generation)
        * [.forEach(func)](#Generation+forEach) ⇒ <code>Promise.&lt;void&gt;</code>
        * [.get(index)](#Generation+get) ⇒ <code>Promise.&lt;any&gt;</code>
        * [.group(func)](#Generation+group) ⇒ [<code>Generation</code>](#Generation)
        * [.has(element)](#Generation+has) ⇒ <code>Promise.&lt;boolean&gt;</code>
        * [.map(func)](#Generation+map) ⇒ [<code>Generation</code>](#Generation)
        * [.reduce(func, init)](#Generation+reduce) ⇒ <code>Muatgen</code>
        * [.toArray()](#Generation+toArray) ⇒ <code>Promise.&lt;Array.&lt;any&gt;&gt;</code>
        * [.unique()](#Generation+unique) ⇒ [<code>Generation</code>](#Generation)
        * [.until(cond)](#Generation+until) ⇒ [<code>Generation</code>](#Generation)
        * [.while(cond)](#Generation+while) ⇒ [<code>Generation</code>](#Generation)
    * _static_
        * [.Generation](#Generation.Generation)
            * [new Generation(generator)](#new_Generation.Generation_new)

<a name="new_Generation_new"></a>

### new Generation()
`Generation`: A smart way to deal with generators.

<a name="Generation+apply"></a>

### generation.apply(args) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that applies certain arguments to its generator.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The applied generation.  

| Param | Type |
| --- | --- |
| args | <code>Array.&lt;any&gt;</code> | 

<a name="Generation+call"></a>

### generation.call() ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that calls its generator with certain arguments.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The called generation.  

| Param | Type |
| --- | --- |
| ...args | <code>Array.&lt;any&gt;</code> | 

<a name="Generation+each"></a>

### generation.each(n) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that groups its yields in arrays of certain length.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The grouper generation  

| Param | Type |
| --- | --- |
| n | <code>number</code> | 

<a name="Generation+filter"></a>

### generation.filter(func) ⇒ [<code>Generation</code>](#Generation)
Returns a new Generation that yields only the yields that applies to a certain condition.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The conditional generation.  

| Param | Type |
| --- | --- |
| func | <code>function</code> | 

<a name="Generation+first"></a>

### generation.first(n) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that yields only the first `n` yields then breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The limiter generation.  

| Param | Type |
| --- | --- |
| n | <code>number</code> | 

<a name="Generation+forEach"></a>

### generation.forEach(func) ⇒ <code>Promise.&lt;void&gt;</code>
Applies a function on each of the yields and returns a new `Promise` that resolves to `void`
when it breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: <code>Promise.&lt;void&gt;</code> - The break promise  

| Param | Type |
| --- | --- |
| func | <code>function</code> | 

<a name="Generation+get"></a>

### generation.get(index) ⇒ <code>Promise.&lt;any&gt;</code>
Returns a promise that resolves to a certain yield identified by index (as if the `Generation`
is an array) then breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: <code>Promise.&lt;any&gt;</code> - The getter promise.  

| Param | Type |
| --- | --- |
| index | <code>number</code> | 

<a name="Generation+group"></a>

### generation.group(func) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that yields the yields in array groups according to a condition
deciding wheather they are alike.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The grouper generation.  

| Param | Type |
| --- | --- |
| func | <code>function</code> | 

<a name="Generation+has"></a>

### generation.has(element) ⇒ <code>Promise.&lt;boolean&gt;</code>
Returns a new `Promise` that resolves to a boolean value indicating if a certain value or object is
yielded then breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: <code>Promise.&lt;boolean&gt;</code> - The boolean indicator.  

| Param | Type |
| --- | --- |
| element | <code>any</code> | 

<a name="Generation+map"></a>

### generation.map(func) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that yields the returns of a function applied to the yields.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The altered generation.  

| Param | Type |
| --- | --- |
| func | <code>function</code> | 

<a name="Generation+reduce"></a>

### generation.reduce(func, init) ⇒ <code>Muatgen</code>
Returns a new `Generation` that yields reduction of the yields by certain operation once.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: <code>Muatgen</code> - The reducer generation.  

| Param | Type |
| --- | --- |
| func | <code>function</code> | 
| init | <code>any</code> | 

<a name="Generation+toArray"></a>

### generation.toArray() ⇒ <code>Promise.&lt;Array.&lt;any&gt;&gt;</code>
Returns a new `Promise` that resolves to an array representation of the yields when it breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: <code>Promise.&lt;Array.&lt;any&gt;&gt;</code> - The array promise  
<a name="Generation+unique"></a>

### generation.unique() ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that makes sure not to repeat yielded values.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The unified generation.  
<a name="Generation+until"></a>

### generation.until(cond) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that yields until a certain condition is `true` then breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The conditional generation.  

| Param | Type |
| --- | --- |
| cond | <code>function</code> | 

<a name="Generation+while"></a>

### generation.while(cond) ⇒ [<code>Generation</code>](#Generation)
Returns a new `Generation` that yields until a certain condition is `false` then breaks.

**Kind**: instance method of [<code>Generation</code>](#Generation)  
**Returns**: [<code>Generation</code>](#Generation) - The conditional generation.  

| Param | Type |
| --- | --- |
| cond | <code>function</code> | 

<a name="Generation.Generation"></a>

### Generation.Generation
**Kind**: static class of [<code>Generation</code>](#Generation)  
<a name="new_Generation.Generation_new"></a>

#### new Generation(generator)
Creates an instance of `Generation`.


| Param | Type |
| --- | --- |
| generator | <code>GeneratorFunction</code> | 

