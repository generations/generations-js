# Generations

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## node.js

If you use node.js you can:

`const Generation = require('./path/to/Generation)`

## API Documentation

For API documentation (generated using jsdoc2md), [click here](Generation.md)